export default {
  name: "singletonPage",
  title: "Site Settings",
  type: "document",
  __experimental_actions: [
    "create", /* "delete", */ "update", "publish"
  ],
  fields: [
    {
      name: 'title',
      title: 'Title',
      type: 'string',
    },
    {
      name: 'content',
      title: 'Content',
      type: 'blockContent',
    }
  ]
}