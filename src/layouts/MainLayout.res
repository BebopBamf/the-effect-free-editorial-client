module Container = ReactBootstrap.Container
module Navbar = ReactBootstrap.Navbar
module Nav = ReactBootstrap.Nav
module Link = Next.Link

module Navigation = {
  @react.component
  let make = () =>
    <Navbar variant=#light expand=#lg fixed=#top>
      <Container>
        <Container fluid=Bool(true) className="d-flex flex-column">
          <Container fluid=Bool(true)>
            <Navbar.Toggle />
            <Link href="/" passHref=true>
              <Navbar.Brand className="mx-auto" href="">
                {React.string("The Effect Free Editorial")}
              </Navbar.Brand>
            </Link>
          </Container>
          <div>
            <Nav variant=#tabs>
              <Nav.Link href="#stories"> {React.string("Stories")} </Nav.Link>
            </Nav>
          </div>
        </Container>
      </Container>
    </Navbar>
}

@react.component
let make = (~children) => {
  <div> <Navigation /> <div className="mt-5"> children </div> </div>
}
